const getMessage = async () => {
    const anchor = document.getElementById("anchor")
    const url = window.location.href

    let prevChats = document.getElementById("messages").innerHTML
    const response = await fetch(url.concat("get"))
    if (response.ok) {
        const jsonResponse = await response.json();
        let stringIsi = ""
        jsonResponse.data.map(msg => (
            stringIsi = stringIsi.concat(`<div class="message">
            <span class="author">${msg.author}</span>
            <span class="time">${msg.time.slice(0,10)}  ${msg.time.slice(11,msg.time.length-7)}</span>
            <p>${msg.message}</p>
          </div>`)
        ))
        document.getElementById("messages").innerHTML = stringIsi
        if (prevChats != stringIsi){
            anchor.scrollIntoView()
        }
    }
}

$("#input-box").focus(e => {
    e.preventDefault
    $('#input-box').addClass("focus")
})

$("#input-box").focusout(e => {
    e.preventDefault
    $('#input-box').removeClass("focus")
})

$('#send').click(e => {
    e.preventDefault()
    let message = $("#input-box").val()
    let valid_form = message === null || message === "" ? false : true;
    let data = {
        messages: message,
    }

    let url = window.location.href.concat("add/")
    
    if (valid_form) {
        $.ajax({
            url: url,
            method : "post",
            dataType : "json",
            data : data,
            headers: { "X-CSRFToken": getCookie("csrftoken") },
            success: (res) => {
                $("#input-box").val("")
                getMessage()
        },
        error: (res) => {
            alert('Failed to send message')
        }})
  
    } else {
        $('.alert-danger').show()
    }
});

function getCookie(c_name) {
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
};

// Main
$(document).ready(function(){
    getMessage();
    setInterval(() => {
        getMessage()
    }, 1000);
    if(window.location.pathname != "/chat/"){
        clearInterval()
    }
})