from django.urls import path
from . import views
from django.views.decorators.csrf import csrf_exempt

app_name = 'acc'
urlpatterns = [
    path('', views.loginPage, name = 'loginPage'),
    path('logout/', views.logoutUser, name='logout'),
    path('register/', views.register, name = 'register'),
    path('validate_username/<str:username>', views.validate_username, name='validate_username'),
]