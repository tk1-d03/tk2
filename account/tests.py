from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import resolve, reverse
from .views import register, loginPage, logoutUser
from .forms import CreatUserForm
from django.http import HttpRequest
from django.contrib.sessions.backends.db import SessionStore
from django.contrib import messages
from django.contrib.messages.storage.fallback import FallbackStorage

class testAccount(TestCase):
    def test_url_login_ada(self):
        response = Client().get('/login/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_login_ada_templatenya(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login/login.html')
    
    def test_login_views(self):
        found = resolve('/login/')
        self.assertEqual(found.func, loginPage)

    def test_url_register_ada(self):
        response = Client().get('/login/register/')
        self.assertEquals(response.status_code, 200)
    
    def test_url_register_ada_templatenya(self):
        response = Client().get('/login/register/')
        self.assertTemplateUsed(response, 'register/register.html')
    
    def test_regiter_views_ada(self):
        found = resolve('/login/register/')
        self.assertEqual(found.func, register)

    def test_register_berhasil(self):
        Client().post('/login/register/', data={'username' : 'aa','password1' : 'aabbccdd123','password2' : 'aabbccdd123'})
        jumlah = User.objects.filter(username='aa').count()
        self.assertEqual(jumlah, 1)

class TestViewsLoginLogout(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('test',password='12test12',email='test@example.com')

    def test_login_berhasil1(self):
        response = self.client.post('/login/', data = {'username' : 'test', 'password' : '12test12'})
        self.assertRedirects(response, '/teams/')

    def test_login_gagal1(self):
        response = self.client.post('/login/', data = {'username' : 'test', 'password' : 'njkhgh679'})
        self.assertRedirects(response, '/login/')

    def test_logout(self):
        request = HttpRequest()
        request.session = SessionStore(session_key=None)
        response = logoutUser(request)
        self.assertEquals(response.status_code, 302)