from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from .forms import CreatUserForm
from django.contrib import messages
from django.http import JsonResponse
import json
from django.contrib.auth.models import User
from django.http import HttpResponse

def register(request):
    form = CreatUserForm()
    if request.method == 'POST':
        form = CreatUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Account created for {username}!')
            return redirect('auth:loginPage')

    context ={'form':form}
    return render(request,'register/register.html' , context)

def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('teams')
        else:
            messages.error(request, 'Username or password is incorrect')
            return redirect('auth:loginPage')

    return render(request,'login/login.html')

def logoutUser(request):
    logout(request)
    return redirect('auth:loginPage')

def validate_username(request, username):
    data = {
        'is_taken': User.objects.filter(username=username).exists()
    }
    return JsonResponse(data)