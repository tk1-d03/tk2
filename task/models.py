import sys
sys.path.append('..')
from team.models import Team

from django.db import models
# Models
from django.contrib.auth.models import User


# Create your models here.
class Task(models.Model):
    task_name = models.CharField(max_length = 100)
    description = models.TextField(max_length = 1000)
    member = models.ForeignKey(User, on_delete = models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, default=None)
    status = models.CharField(max_length = 50, default = 'on_going')

    # status = submitted --> sudah di submit menunggu di accept
    # status = accepted --> sudah di accept
    # status = on_going --> pas baru dikasih/abis di reject

    def __str__(self):
        return f'{self.task_name} is a task for {self.member}, task description : {self.description}'


