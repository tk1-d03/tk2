from django.test import TestCase, Client, override_settings
from django.urls import reverse, resolve
from .models import Task
from account.views import logoutUser
from team.models import Team
from .forms import TaskForm
from .views import add_task, show_task, submit_task
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.apps import apps
from .apps import TaskConfig
from django.contrib.auth import login, logout, authenticate, get_user
from django.contrib.sessions.backends.db import SessionStore

# Test Model
class Model_Tests(TestCase) :
    def test_model_can_add_new_task(self):
        user = User(username='raniah', password = '12345')
        user.save()
        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        task = Task.objects.create(task_name='task', description = 'task description', member = user, team = team , status='on_going')
        count_tasks = Task.objects.all().count()
        self.assertEqual(count_tasks, 1)
        task_text = Task.objects.first()
        self.assertEquals(str(task_text), 'task is a task for raniah, task description : task description')


# Test Forms
class Forms_Test(TestCase) :
    def test_form_add_task(self) :
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name' : 'task1',
            'description' : 'desc',
            'member' : user,
            'status' : 'on_going',
        }, 
        id = team.id)
        self.assertTrue(task_input.is_valid())

    def test_task_form_validation_task_name(self):
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name': '',
            'description' : 'desc',
            'member' : user,
            'status' : 'on_going',
            }, 
            id = team.id)
        self.assertFalse(task_input.is_valid())
        self.assertEqual(
            task_input.errors['task_name'],
            ["This field is required."]
        )

    def test_task_form_validation_description(self):
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name': 'task1',
            'description' : '',
            'member' : user,
            'status' : 'on_going',
            }, 
            id = team.id)
        self.assertFalse(task_input.is_valid())
        self.assertEqual(
            task_input.errors['description'],
            ["This field is required."]
        )

    def test_task_form_validation_member(self):
        user = User(username='raniah', password = '12345')
        user.save()

        team = Team(team_owner=user, name='team1', description='team1')
        team.save()
        team.members.add(user)

        task_input = TaskForm(data = {
            'task_name': 'task1',
            'description' : 'desc',
            'member' : '',
            'status' : 'on_going',
            }, 
            id = team.id)
        self.assertFalse(task_input.is_valid())
        self.assertEqual(
            task_input.errors['member'],
            ["This field is required."]
        )


# Test URLs
class Urls_Tests(TestCase) :
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create(
            username="raniah",
            password='12345',
            email ='rania@gmail.com'
        )
        self.user.save()
        self.client.login(username="raniah", password="12345")
        self.status = self.client.login(username="raniah", password="12345")
        self.team = Team.objects.create(
            team_owner=self.user, 
            name='team1', 
            description='team1',
        )
        self.team.save()
        self.task = Task.objects.create(
            task_name = 'task1',
            description = 'description',
            member = self.user,
            team = self.team,
        )
        self.add_task = reverse("task:add_task", args=[self.team.id])
        self.employee = reverse('task:employee', args=[self.team.id])
        self.submit_task = reverse('task:submit_task', args=[self.team.id, self.task.id])

    def test_add_task_use_the_right_function(self):
        found = resolve(self.add_task)
        self.assertEqual(found.func, add_task)

    def test_employee_use_the_right_function(self):
        found = resolve(self.employee)
        self.assertEqual(found.func, show_task)
    
    def test_submit_task_use_the_right_function(self):
        found = resolve(self.submit_task)
        self.assertEqual(found.func, submit_task)

    def test_not_logged_in(self):
        response = Client().get(f'/team/{self.team.id}/task/add')
        self.assertEquals(response.status_code, 301)

        response = Client().get(f'/team/{self.team.id}/task/employee')
        self.assertEquals(response.status_code, 301)

#Test Views and Templates
@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class Views_and_Templates_Tests(TestCase) :
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='raniah', password='12345', email ='rania@gmail.com')
        self.user.save()
        self.team = Team(
            team_owner=self.user, 
            name='team1', 
            description='team1',
        )
        self.team.save()
        user2 = User.objects.create_user(username='wawa', password = 'wawa123', email ='wawa@gmail.com')
        user2.save()
        self.team.members.add(user2)
        self.task = Task.objects.create(
            task_name = 'task1',
            description = 'description',
            member = self.user,
            team = self.team,
        ) 
        self.task.save()
        self.add_task = reverse("task:add_task", args=[self.team.id])
        self.employee = reverse('task:employee', args=[self.team.id])

    def test_template_for_add_task(self):
        self.client.login(username='raniah', password='12345')
        response = self.client.get(self.add_task)
        self.assertTemplateUsed(response, 'taskForm.html')
        self.assertEqual(response.status_code, 200)

    def test_add_task_logout(self) : 
        self.client.login(username='raniah', password='12345')
        self.client.logout()
        response = self.client.get(self.add_task)
        self.assertEqual(response.status_code, 302)

    def test_template_for_employee_dashboard(self):
        self.client.login(username='raniah', password='12345')
        response = self.client.get(self.employee)
        self.assertTemplateUsed(response, 'employee.html')
        self.assertEqual(response.status_code, 200)
    
    def test_employee_logout(self) : 
        self.client.login(username='raniah', password='12345')
        self.client.logout()
        response = self.client.get(self.employee)
        self.assertEqual(response.status_code, 302)
    
    def test_add_task(self) :
        self.client.login(username='raniah', password='12345')
        response = self.client.post(self.add_task,
        {
            'task_name': 'task2',
            'description' : 'desc',
            'member' : 1,
        }, 
        follow=True)
        self.assertEqual(response.status_code, 200)

    # def test_add_task_bad_response(self) :
    #     self.client.login(username='raniah', password='12345')
    #     response = self.client.post(self.add_task,
    #     {
    #         'task_name': 'task2',
    #         'description' : 'desc',
    #         'member' : self.user,
    #     }, 
    #     follow=True)
    #     self.assertEqual(response.status_code, 400)

    def test_logged_in(self) : 
        status = self.client.login(username='raniah', password='12345')
        self.assertEqual(status, True)
        
    def test_login_succesful(self):
        user = authenticate(username='raniah', password='12345')
        self.assertTrue((user is not None) and user.is_authenticated)

    def test_login_not_successful(self):
        user = authenticate(username='rania', password='123')
        self.assertFalse((user is not None) and user.is_authenticated)

class TestApp(TestCase):
    def test_apps(self):
        self.assertEqual(TaskConfig.name, 'task')
        self.assertEqual(apps.get_app_config('task').name, 'task')
