from django.shortcuts import render,redirect
from django.http import HttpResponseBadRequest
from .models import Task
from .forms import TaskForm
from . import forms
from team.models import Team
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

# Create your views here.
@login_required(login_url="/login/")
def add_task(request, id) :
    task_form = forms.TaskForm(id)
    team_instance = Team.objects.get(id=id)
    members = team_instance.members.all()
    try :
        team = Team.objects.get(id=id)
    except Team.DoesNotExist :
        return HttpResponseBadRequest()
    
    if request.method == "POST":
        try :
            task_name = request.POST.get('task_name')
            description = request.POST.get('description')
            member = request.POST.get('member')
        except KeyError:
            return HttpResponseBadRequest
        
        try:
            member = User.objects.get(id=member)
        except:
            return HttpResponseBadRequest()

        task = Task(task_name = task_name, description = description, member = member, team = team)
        task.save()
        return JsonResponse({
            'success': True
        })
    else:
        current_data = Task.objects.filter(member=request.user).filter(status = 'on_going')
        return render(request, 'taskForm.html' ,{'form': task_form,'data':current_data, "team": team_instance, "member_list": members, 'team_id' : id })

@login_required(login_url="/login/")
def show_task(request, id) :
    task = Task.objects.filter(team = Team.objects.get(id=id)).filter(member=request.user).filter(status = 'on_going')
    team_instance = Team.objects.get(id=id)
    members = team_instance.members.all()
    response = {
        'tasks': task,
        "team": team_instance,
        "member_list": members,
        "team_id" : id,
    }
    return render(request,'employee.html', response)
    
@login_required(login_url="/login/")
def submit_task(request, id, pk) :
    if request.method == 'POST' :
        task = Task.objects.filter(team = Team.objects.get(id=id)).filter(member=request.user).get(id=pk)
        task.status = 'submitted'
        task.save()
        return redirect(f'/team/{id}/task/employee/')