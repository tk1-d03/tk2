from django import forms
from .models import Task
from django.contrib.auth.models import User

from team.models import Team

class TaskForm(forms.Form):        
    task_name = forms.CharField(
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control-lg',
                'type' : 'text',
                'required': True,
                'autocomplete' : 'off',
                'placeholder' : 'Task Name',
            }
        )
    )

    description = forms.CharField (
        max_length = 1000,
        widget = forms.TextInput(
            attrs={
                'class':'form-control-lg',
                'type' : 'text',
                'autocomplete' : 'off',
                'required' : True,
            }
        )
    )

    member = forms.ModelChoiceField(
        queryset= Team.objects.all(),
        widget = forms.Select(
            attrs={
                'class':'form-control-lg',
                'required' : True,
            }
        )
    )

    def __init__(self, id, *args, **kwargs):
        self.id = kwargs.pop('id', None)
        super().__init__(*args, **kwargs)
        self.fields['member'].queryset = Team.objects.get(id=id).members.all()



