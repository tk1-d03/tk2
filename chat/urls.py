from django.urls import path
from . import views

app_name = 'chat'

urlpatterns = [
    path('', views.chat, name = 'index'),
    path('get/', views.get, name='get'),
    path('add/', views.add_message, name='add')
]