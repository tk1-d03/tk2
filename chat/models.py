from django.db import models
from django.db.models.fields.related import ForeignKey
from django.contrib.auth.models import User
import sys
sys.path.append('..')
from team.models import Team

# Create your models here.
class Message(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE, default=None)
    message = models.TextField();   
    time = models.DateTimeField();

    def __str__(self):
        return f'Posted on {self.team} at {self.time} by {self.author}: {self.message}'

