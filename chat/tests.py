from django.contrib.auth import authenticate
from django.http import HttpRequest
from django.test import TestCase, Client, override_settings
from .models import Message
from django.contrib.auth.models import User
from django.utils import timezone
from team.models import Team
from django.urls import reverse
from django.utils.encoding import force_text

from .views import get, add_message

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class Test(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username="test", password="123456", email="test@a.com")
        self.user.save()

        self.team_instance = Team(team_owner=self.user, name='name', description='description')
        self.team_instance.save()

        self.time = timezone.now()
        self.message = Message.objects.create(author=self.user, team=self.team_instance, message="test message", time=self.time)
        self.message.save()

        self.chat_url = reverse('chat:index', args=[self.team_instance.id])
        self.get_chat_url = reverse('chat:get', args=[self.team_instance.id])
        self.add_chat_url = reverse('chat:add', args=[self.team_instance.id])
    
    def test_model_message(self):        
        hitung = Message.objects.all().count()
        self.assertEquals(hitung, 1)

        self.assertEquals(str(self.message), f"Posted on {self.team_instance} at {self.time} by {self.user}: test message")

    def test_templates_used(self):
        self.client.login(username="test", password="123456")
        response = self.client.get(self.chat_url)
        self.assertTemplateUsed(response, 'chat/chats.html')

    def test_url_chat_not_logged_in(self):
        response = self.client.get(self.chat_url)
        self.assertEquals(response.status_code, 302)

    def test_add_url_chat_not_logged_in(self):
        response = self.client.get(self.add_chat_url)
        self.assertEquals(response.status_code, 302)

    def test_get_url_chat_not_logged_in(self):
        response = self.client.get(self.get_chat_url)
        self.assertEquals(response.status_code, 302)

    def test_url_chat_logged_in(self):
        self.client.login(username="test", password="123456")
        response = self.client.get(self.chat_url)
        self.assertEquals(response.status_code, 200)

    def test_add_url_chat_logged_in(self):
        self.client.login(username="test", password="123456")
        response = self.client.get(self.add_chat_url)
        self.assertEquals(response.status_code, 200)

    def test_get_url_chat_logged_in(self):
        self.client.login(username="test", password="123456")
        response = self.client.get(self.get_chat_url)
        self.assertEquals(response.status_code, 200)

    def test_get_message_logged_in(self):
        self.client.login(username="test", password="123456")
        request = HttpRequest()
        request.user = authenticate(request, username='test', password='123456')
        request.method = 'GET'
        response = get(request, self.user.id)
        self.assertJSONEqual(str(response.content, encoding="utf8"), {"data":[{"id":self.message.id, "author":str(self.message.author), "message":str(self.message.message), "time":(str(self.message.time)[0:10] + "T" + str(self.message.time)[11:-3])}]})

    def test_add_message_logged_in(self):
        self.client.login(username="test", password="123456")
        request = HttpRequest()
        request.user = authenticate(request, username='test', password='123456')
        request.method = 'POST'
        request.POST['messages'] = "message text"
        response = add_message(request, self.user.id)
        self.assertJSONEqual(force_text(response.content), {'success': True})