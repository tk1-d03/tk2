from django.test import TestCase, Client, override_settings
from django.urls import resolve, reverse
from django.http import HttpRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .views import *
from task.models import Task
from team.models import Team

# Create your tests here.

@override_settings(STATICFILES_STORAGE='django.contrib.staticfiles.storage.StaticFilesStorage')
class TestDashM(TestCase):

    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user('jsaon', password='abcdefgh', email='jsaon@gmail.com')
        self.user.save()
        self.client.login(username='jsaon', password='abcdefgh')
        self.team = Team.objects.create(
            team_owner=self.user,
            name='team1',
            description='test',
        )
        self.team.save()
        self.task = Task.objects.create(
            task_name='task1',
            description='description1',
            member=self.user, 
            team=self.team, 
            status='on_going',
        )
        self.task.save()
        self.dashboard_url = reverse('dashM:manager', args=[self.team.id])
        self.task_accept_url = reverse('dashM:task_accept', args=[self.team.id])
        self.task_reject_url = reverse('dashM:task_reject', args=[self.team.id])
        self.task_delete_url = reverse('dashM:task_delete', args=[self.team.id])

    def test_landing_page(self):
        self.assertIsNotNone(dashboard)

    def test_url_ada(self):
        self.client.login(username='jsaon', password='abcdefgh')
        response = self.client.get(self.dashboard_url)
        self.assertEqual(response.status_code, 200)

    def test_template(self):
        self.client.login(username='jsaon', password='abcdefgh')
        response = self.client.get(self.dashboard_url)
        self.assertTemplateUsed(response, 'dashMn.html')
        self.assertEqual(response.status_code, 200)

    def test_template_logout(self):
        self.client.login(username='jsaon', password='abcdefgh')
        self.client.logout()
        response = self.client.get(self.dashboard_url)
        self.assertEqual(response.status_code, 302)
    
    def test_bad_request(self):
        self.client.login(username='jsaon', password='abcdefgh')
        response = self.client.post(self.dashboard_url, data={'user':self.user, 'team_instance':self.team})
        self.assertEqual(response.status_code, 400)

    def test_relasi_dashboard_funct(self):
        self.client.login(username='jsaon', password='abcdefgh')
        found = resolve(self.dashboard_url)
        self.assertEqual(found.func, dashboard)

    def test_relasi_task_accept_funct(self):
        self.client.login(username='jsaon', password='abcdefgh')
        found = resolve(self.task_accept_url)
        self.assertEqual(found.func, task_accept_ajax)

    def test_relasi_task_reject_funct(self):
        self.client.login(username='jsaon', password='abcdefgh')
        found = resolve(self.task_reject_url)
        self.assertEqual(found.func, task_reject_ajax)

    def test_relasi_task_delete_funct(self):
        self.client.login(username='jsaon', password='abcdefgh')
        found = resolve(self.task_delete_url)
        self.assertEqual(found.func, task_delete_ajax)

    def test_accept(self):
        self.client.login(username='jsaon', password='abcdefgh')
        counter_ongoing = Task.objects.filter(status='on_going').count()
        counter_accept = Task.objects.filter(status='accepted').count()
        request = HttpRequest()
        request.user = authenticate(request, username='jsaon', password='abcdefgh')
        request.method = 'GET'
        response = task_accept(request, self.user.id, self.task.id)
        self.assertEqual(Task.objects.filter(status='on_going').count(), counter_ongoing-1)
        self.assertEqual(Task.objects.filter(status='accepted').count(), counter_accept+1)

    def test_reject(self):
        self.client.login(username='jsaon', password='abcdefgh')
        request = HttpRequest()
        request.user = authenticate(request, username='jsaon', password='abcdefgh')
        request.method = 'GET'
        task_accept(request, self.user.id, self.task.id)
        counter_accept = Task.objects.filter(status='accepted').count()
        response = task_reject(request, self.user.id, self.task.id)
        self.assertEqual(Task.objects.filter(status='accepted').count(), counter_accept-1)

    def test_delete(self):
        self.client.login(username='jsaon', password='abcdefgh')
        counter_ongoing = Task.objects.filter(status='on_going').count()
        request = HttpRequest()
        request.user = authenticate(request, username='jsaon', password='abcdefgh')
        request.method = 'GET'
        response = task_delete(request, self.user.id, self.task.id)
        self.assertEqual(Task.objects.filter(status='on_going').count(), counter_ongoing-1)