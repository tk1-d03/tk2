from django.urls import path
from . import views

app_name = "dashM"

urlpatterns = [
	path('', views.dashboard, name = 'manager'),
	path('accept/', views.task_accept_ajax, name='task_accept'),
	path('reject/', views.task_reject_ajax, name='task_reject'),
	path('delete/', views.task_delete_ajax, name='task_delete')
]
