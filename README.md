# Tugas Kelompok 1

[![pipeline status](https://gitlab.com/tk1-d03/tk2/badges/master/pipeline.svg)](https://gitlab.com/tk1-d03/tk2/-/pipelines)
[![coverage report](https://gitlab.com/tk1-d03/tk2/badges/master/coverage.svg?job=UnitTest)](https://gitlab.com/tk1-d03/tk2/-/commits/master)

#### Nama-nama Anggota Kelompok
| No. | Nama                      | NPM        |
|-----|---------------------------|------------|
| 1   | Etya Resa Fatma           | 1906293026 |
| 2   | Hanif Arkan Audah         | 1906293070 |
| 3   | Johanes Jason             | 1906293120 |
| 4   | Muhammad Fathan Muthahhari| 1906293190 |
| 5   | Raniah Nur Hanami         | 1906293291 |

#### Link Herokuapp
https://romusha-2.herokuapp.com/

#### Cerita Aplikasi Yang Diajukan Serta Kebermanfaatannya

Aplikasi yang dibuat adalah suatu *team management software* dimana seorang manajer dapat memberikan *task* serta berkomunikasi mengenai pekerjaan secara langsung dengan anggota team yang lain. Hal tersebut dapat dilakukan dengan dua langkah sederhana:
1. Membuat team
2. Menambahkan beberapa rekan kerja sebagai *member* dari team tersebut.

Aplikasi ini membantu penggunanya dalam mengatur pembagian tugas untuk keperluan kerja sehingga bermanfaat untuk perkantoran yang terpaksa bekerja dari rumah akibat pandemi covid-19.

#### Daftar Fitur Yang Akan Diimplementasikan
1. Login dan register   : Etya Resa Fatma
2. User dashboard       : Hanif Arkan Audah
3. Create team          : Hanif Arkan Audah
4. Sidebar dan navbar   : Hanif Arkan Audah
5. Employee dashboard   : Raniah Nur Hanami
6. Create task          : Raniah Nur Hanami
7. Manager Dashboard    : Johanes Jason
8. Chat                 : Muhammad Fathan Muthahhari
