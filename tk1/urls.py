"""tk1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls import include, path

from .views import index

# views
from team import views as team_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name = 'index'),
    path('login/',include('account.urls')),
    path('dashboard/', team_views.dashboard),
    # path('task/', include('task.urls')),
    path('', index, name = 'home'),
    path('teams/', team_views.dashboard, name='teams'),
    path('create-team/', team_views.create_team, name='create-team'),
    path('delete-team/<int:pk>/', team_views.delete_team, name='delete-team'),
    path('ajax-add-team/', team_views.add_team, name='ajax-add-team'),
    path('login/',include('account.urls', namespace='auth')),
    path('team/<int:id>/task/', include('task.urls', namespace="team")),
    path('team/<int:id>/chat/', include('chat.urls', namespace='chat')),
    path('dashM/<int:id>/', include('dashM.urls', namespace='manager'))
]
