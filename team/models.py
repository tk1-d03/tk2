# Django Imports
from django.db import models

# Models
from django.contrib.auth.models import User

class Team(models.Model):
  team_owner = models.ForeignKey(User, on_delete=models.CASCADE, related_name='owned_teams')
  name = models.CharField(max_length=128)
  description = models.TextField()
  members = models.ManyToManyField(User)

  def __str__(self):
    return f'{self.name} is a team owned by {self.team_owner}'

# How to interact with the Team model

# 1. Create Team:
#   team_instance = Team(team_owner=user, name='name', description='description')
#   team_instance.save()

# 2. Add Member:
#   team_instance.members.add(user)

# 3. Check all team members:
#   team_instance.members.all()

# 4. Check all teams owned by user:
#   user.owned_teams.all()

# 5. Check all teams that user is a member of:
#   user.team_set.all()

